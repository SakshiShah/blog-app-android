package com.example.blogapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.blogapp.adapter.MainAdapter
import com.example.blogapp.databinding.ActivityMainBinding
import com.example.blogapp.http.Blog
import com.example.blogapp.http.BlogHttpClient
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_blog_detail.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myAdapter = MainAdapter {blog ->
        BlogDetailsActivity.start(this, blog)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        startActivity(Intent(this, BlogDetailsActivity::class.java))

        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = myAdapter

        binding.refresh.setOnRefreshListener {
            loadData()
        }
        loadData()
    }

    private fun loadData(){
        binding.refresh.isRefreshing = true
        BlogHttpClient.loadBlogArticles(
                onSuccess = { blogList: List<Blog> ->
                    runOnUiThread {
                        binding.refresh.isRefreshing = false
                        myAdapter.submitList(blogList)
                    }
                },
                onError = {
                    binding.refresh.isRefreshing = false
                    runOnUiThread{ showErrorSnackbar() }
                }
        )
    }

    private fun showErrorSnackbar() {
        binding.refresh.isRefreshing = true
        val snackbar = Snackbar.make(snackbarPos,
                "Error while loading article!",
                Snackbar.LENGTH_INDEFINITE).run {
            setActionTextColor(resources.getColor(R.color.orange_500))
            setAction("Retry") {
                loadData()
                dismiss()
            }
        }.show()
    }
}