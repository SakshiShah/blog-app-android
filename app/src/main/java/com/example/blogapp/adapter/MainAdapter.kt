package com.example.blogapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.DrawableTransformation
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.blogapp.databinding.ItemMainBinding
import com.example.blogapp.http.Blog

/**
 * General Working of Recycler View:
 * 1. It executes "onCreateViewHolder the number of times which is equal to the number of list items that can appear on the screen at the same time!
 * 2. It executes "onBindViewHolder" every time we need to render a list item
 * 3. When a user scrolls the list, views that are not visible anymore gets reused with a corressponding view holder.
  */

class MainAdapter(private val onItemClickListener: (Blog) -> Unit): ListAdapter<Blog, MainViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        // This method is called only a certain amount of times when the recycler view needs a new view holder object
        val inflator = LayoutInflater.from(parent.context)
        val binding = ItemMainBinding.inflate(inflator, parent, false)

        return  MainViewHolder(binding, onItemClickListener)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        // This method is called every time when we need render a list item (bind the model with view holder)

        holder.bindTo(getItem(position))
    }

}

class MainViewHolder(private val binding: ItemMainBinding, private val onItemClickListener: (Blog) -> Unit): RecyclerView.ViewHolder(binding.root){
    fun bindTo(blog: Blog) {
        binding.root.setOnClickListener{ onItemClickListener(blog) }
        binding.textTitle.text = blog.title
        binding.textDate.text = blog.date

        Glide.with(itemView)
            .load(blog.author.getAvatarImage())
            .transform(CircleCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into((binding.imageAvatar))
    }
}

/*
 * If we want our listAdapter to work correctly, then we need to implement DiffUtil.ItemCallback
 * This is a special callback that is used by the adapter to figure out of the items are the same!
 * If the item content is the same for the further difference calculation, it will re-render only items that have changed!
 *
 * DiffUtil.ItemCallback has two methods:
 * 1. areItemTheSame: where we compare two objects of type T
 * 2. areContentsTheSame: where we compare two objects of type T
 */

private val DIFF_CALLBACK: DiffUtil.ItemCallback<Blog> = object: DiffUtil.ItemCallback<Blog>() {
    override fun areItemsTheSame(oldItem: Blog, newItem: Blog): Boolean {
        return  oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Blog, newItem: Blog): Boolean {
        return oldItem == newItem
    }
}